package game;

import java.util.Scanner;

public class Game{
    Scanner sc = new Scanner(System.in);
    private int numberOfPlayers;
    private Player[] players;
    private Status gameState = Status.SETUP;


    public int getMaxScore() {
        return 100;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(int numberOfPlayers){
        this.numberOfPlayers = numberOfPlayers;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public Status getGameState() {
        return gameState;
    }

    public void setGameState(Status gameState) {
        this.gameState = gameState;
    }

    public void setup(){
        System.out.println("\nWelcome to the dice game!\nHow many players are playing?");
        setPlayers(new Player[sc.nextInt()]);

        for (int i = 0; i < players.length; i++) {
            System.out.printf("Insert player%d name:\n", i + 1);
            players[i] = new Player(sc.next(), i);
        }
        setNumberOfPlayers(players.length);
        setGameState(Status.START);
    }


    public void startGame() {
        for (int i = 0; i < players.length; ) {
            if (players[i].getPlayerId() == i) {
                while (players[i].getPoints() < getMaxScore()) {
                    int roll = Player.diceRoll();
                    players[i].addPoints(roll);
                    System.out.printf("%s rolled and got %d, Points: %d\n", players[i].getName(), roll, players[i].getPoints());
                    if (players[i].getPoints() >= getMaxScore()) {
                        System.out.printf("Game over, %s won!\n", players[i].getName());
                        setGameState(Status.GAME_OVER);
                        break;
                    }
                    if (roll < 6) {
                        if (i < getNumberOfPlayers() - 1) {
                            i++;
                        } else {
                            i = 0;
                        }
                    }
                }
                break;
            }
        }
    }

    public void restart(){
        System.out.println("\nWould you like to start a new game?\nYes(Y), No(n)");
        String answer = sc.next();
        if(answer.equals("Yes") || answer.equals("Y")){
            setGameState(Status.SETUP);
        } else {
            System.out.println("Thank you for playing!\nSee you next time!");
            setGameState(Status.EXIT);
        }
    }
}
