package game;

public enum Status {
    SETUP,
    START,
    GAME_OVER,
    EXIT
}
