package game;

public class Player {
    private final int playerId;
    private final String name;
    private int points = 0;


    public Player(String name, int playerId) {
        this.name = name;
        this.playerId = playerId;

    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void addPoints(int points) {
        this.points += points;
    }


    public static int diceRoll() {
        return (int) Math.floor(Math.random() * 6 + 1);
    }
}
