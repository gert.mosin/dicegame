import game.Game;
import game.Status;

public class DiceGame {
    public static void main(String[] args) {

        Game game = new Game();

        while (game.getGameState() != Status.EXIT){
            switch (game.getGameState()){
                case SETUP: {
                    game.setup();
                }
                case START: {
                    game.startGame();
                }
                case GAME_OVER:
                    game.restart();
                case EXIT: break;
                default:
                    System.out.println("Something went wrong!");
                    break;
            }
        }


    }
}
